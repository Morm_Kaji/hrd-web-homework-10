import React, { useState, useEffect } from "react";
import { Col, Container, Row, Form, Button, Table } from "react-bootstrap";
import { deleteArticle, editArticle, fetchArticle, postArticle, uploadImage } from "../services/article_service";

function Auth() {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const [imageFile, setImageFile] = useState(null)
    const [articles, setArticles] = useState([]);
    const [btn , setBtn]=useState("Add");
    const [author, setAuthor] = useState("Author Name");
    const [email, setEmail] = useState("Email");

    const onAdd = async (e) => {
        e.preventDefault()
        let article = {
            title, description
        }
        if (imageFile) {
            let url = await uploadImage(imageFile)
            article.image = url
        }
        postArticle(article).then(message => alert(message))
        
    }


    useEffect(() => {
        const fetch = async () => {
            let articles = await fetchArticle();
            setArticles(articles);
        };
        fetch();
    }, []);


    const onDelete = (id) => {
        deleteArticle(id).then((message) => {

            let newArticles = articles.filter((article) => article._id !== id)
            setArticles(newArticles)

            alert(message)
        }).catch(e => console.log(e))
    }
    

    const onEdit = async (id) =>{
        // console.log("ID ",id)
        // let article = {
        //     title, description
        // }
        // if (imageFile) {
        //     let url = await uploadImage(imageFile)
        //     article.image = url
        // }
        // editArticle(article).then(message => alert(message))
    }

    return (
        <Container>
            <h1 className="my-2">New Article</h1>
            <Row>
                <Col md={8}>
                    <Form>
                        <Form.Group controlId="title">
                            <Form.Label>Author Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder={author}
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group controlId="description">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder={email}
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                            />
                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={onAdd}
                        >
                            Save
                </Button>
                    </Form>
                </Col>
                <Col md={4}>
                    <img className="w-100" src={imageURL} />
                    <Form>
                        <Form.Group>
                            <Form.File
                                id="img"
                                label="Choose Image"
                                onChange={(e) => {
                                    let url = URL.createObjectURL(e.target.files[0])
                                    setImageFile(e.target.files[0])
                                    setImageURL(url)
                                }}
                            />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        articles.map((item, index) => (
                            <tr key={index}>
                                <td>{index}</td>
                                <td>{item.title}</td>
                                <td>{item.description}</td>
                                <td><img
                                style={{ objectFit: "cover", height: "100px" }}
                                src={item.image ? item.image:"https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}></img></td>
                                <td>
                                    <Button variant="warning" onClick={()=> onEdit(index)}>Edit</Button>
                                    <Button variant="danger" onClick={() => onDelete(item._id)}>Delete</Button>
                                </td>
                            </tr>
                        ))
                    }

                </tbody>
            </Table>
        </Container>
    );
}

export default Auth
